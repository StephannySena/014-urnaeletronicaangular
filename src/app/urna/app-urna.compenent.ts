import { Component, Output, EventEmitter } from "@angular/core";
import { DadosService } from "../service/dados.service";

@Component({
    selector: "app-urna",
    templateUrl: "./app-urna.component.html",
    styleUrls: ["./app-urna.component.scss"]
})

export class AppUrna{

  constructor(private service: DadosService) {}

    //@Output() aoVotar = new EventEmitter<any>();

    rgEleitor: string = "Insira seu RG";
    candidato: string = "";
    data: string = "";
    public votar(){
        console.log("RG do eleitor:" + this.rgEleitor );
        console.log("Candidato:" + this.candidato);

        const voto = {rg: this.rgEleitor, candidato: this.candidato}

        //this.aoVotar.emit(voto)
        this.service.addVoto(voto).subscribe( resultado => {
          console.log(resultado);
          this.limparTela()

        })


    }

    public limparTela(){
        this.rgEleitor = "";
        this.candidato = ""
    }



}
