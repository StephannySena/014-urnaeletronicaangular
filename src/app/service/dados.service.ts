import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Voto } from 'models/voto.models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DadosService {

  private _listaVotos: any[] = [];
  private url = "http://localhost:3000/votos"

  constructor(private httpClient: HttpClient) { }

public get listaVotos(): any[] {
  return this._listaVotos;
}
public set listaVotos(value: any[]) {
  this._listaVotos = value;
}

public getAllVotos(): Observable<Voto[]>{
  return this.httpClient.get<Voto[]>(this.url)
}

public addVoto(voto: any) : Observable<Voto>{

  voto.data = new Date()
  console.log(voto);

 // this.listaVotos.push(voto)
  return this.httpClient.post<Voto>(this.url, voto)


}


}
