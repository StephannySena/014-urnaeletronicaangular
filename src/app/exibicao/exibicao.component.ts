import { Component, OnInit, Input } from '@angular/core';
import { Voto } from 'models/voto.models';
import { DadosService } from '../service/dados.service';

@Component({
  selector: 'app-exibicao',
  templateUrl: './exibicao.component.html',
  styleUrls: ['./exibicao.component.scss']
})
export class ExibicaoComponent implements OnInit {

  constructor(private service: DadosService) {}


  votos: any = [];


  ngOnInit(): void {

    this.votos = this.service.listaVotos

    this.service.getAllVotos().subscribe((votos:Voto[]) => {
      console.table(votos);
      this.votos = votos

    })

  }

}
