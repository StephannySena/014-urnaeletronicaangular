import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppUrna } from './urna/app-urna.compenent';
import { ExibicaoComponent } from './exibicao/exibicao.component';

@NgModule({
  declarations: [
    AppComponent,
    AppUrna,
    ExibicaoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
